package demo.pos;

import demo.pos.common.Interval;
import demo.pos.domain.sale.discount.BigSpenderDiscount;
import demo.pos.domain.sale.discount.SaleDiscount;
import demo.pos.persistence.Repositories;
import demo.pos.domain.product.ProductDescription;
import demo.pos.business.ProductService;
import demo.pos.application.PosController;
import demo.pos.domain.sale.Sale;
import demo.pos.persistence.PersistenceType;
import org.junit.jupiter.api.*;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;
/**
 * Created by overvelj on 14/11/2016.
 */
 class POSTest {
   private PosController r = new PosController();
   private ProductService catalog = new ProductService();

	//   De @BeforeEach method is used to initialise the system for tests.
    @BeforeEach
     void setUp() {
      // clears the datastore
      // ONLY THE MEMORY FACTORY IS FUNCTIONAL
        Repositories.init(PersistenceType.MEMORY);
	    // Create Testdata (fixture)). Never do this in the domain/production code!
        ProductDescription pd1 = new ProductDescription( 1,"Een", 1.2);
        ProductDescription pd2 = new ProductDescription(2,"Twee", 3.54);
        catalog.addProduct(pd1);
        catalog.addProduct(pd2);

    }

    // All testmethods are annotated with @Test
    @Test
     void createNewSaleTest() {
        Sale s = r.makeNewSale();
        // In testmethods use Asserts to test if the system behaves as expected.
        // Wat to test:
        //   -  postconditions of the operation contract
        //   -  return value.
        //   - Verification that all steps were correctly executed.
        assertNotNull(s);
        assertNotNull(s.getSalesLineItems());
        assertFalse(s.isComplete());
    }

    @Test
     void addItemTest() {
       long saleId =  r.makeNewSale().getId(); // step 1
        r.enterItem(saleId,1,10); // step 2
        assertEquals(1,
                r.getSale(saleId).getSalesLineItems().get(0).getPd().getProductId());
    }

    @Test
     void endSaleTest() {
       long saleId =  r.makeNewSale().getId(); // step 1
        r.enterItem(saleId,1,10);
        r.endSale(saleId);
	    assertTrue(r.getSale(saleId).isComplete());
    }

    @Test
     void getTotalTest() {
       long saleId =  r.makeNewSale().getId(); // step 1
        r.enterItem(saleId,1,10);
        r.enterItem(saleId,2,5);
        r.endSale(saleId);
	    assertEquals((10*1.2)+(5*3.54),r.getSale(saleId).getTotal());
    }

    @Test
     void makePaymentTest() {
       long saleId =  r.makeNewSale().getId(); // step 1
       r.enterItem(saleId,1,10);
       r.enterItem(saleId,2,5);
       r.endSale(saleId);
        r.makePayment(saleId,30);
	    assertEquals(30,r.getSale(saleId).getPayment().getAmount());
	    assertEquals(1,r.getStore().countSales());
    }

  @Test
  void DiscountTest() {
    long saleId =  r.makeNewSale().getId(); // step 1
    r.enterItem(saleId,1,10);
    r.enterItem(saleId,2,5);
    r.addDiscount(saleId,new BigSpenderDiscount(27,-5));
    r.addDiscount(saleId,new SaleDiscount(new Interval(LocalDate.now().minusDays(5),LocalDate.now().plusDays(5)),-0.1));
    assertEquals(((10*1.2)+(5*3.54)-5)*0.9,r.getSale(saleId).getTotal(),0.01,"Error calculating discount");
  }
}
