package demo.pos.domain.sale.discount;

import demo.pos.domain.sale.Sale;

/**
 * @author Jan de Rijke.
 */
public interface Discount {
	double getDiscount(double amount);
}
