package demo.pos.domain.sale;

import demo.pos.domain.product.ProductDescription;

/**
 * Created by overvelj on 14/11/2016.
 */
public class SalesLineItem {
    private ProductDescription pd;
    private int qty;

    public ProductDescription getPd() {
        return pd;
    }

    public void setPd(ProductDescription pd) {
        this.pd = pd;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public SalesLineItem(ProductDescription pd, int qty) {

        this.pd = pd;
        this.qty = qty;
    }

    public double getSubTotal() {
        return pd.getPrice()*qty*1;
    }
}
