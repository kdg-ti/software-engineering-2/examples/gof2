package demo.pos.persistence;

import demo.pos.domain.sale.Sale;

/**
 * Dit is een dummy implementatie van een repository dat naar een DB schrijft
 */
public class SaleDbRepository implements SaleRepository {

     SaleDbRepository() {
    }

    @Override
    public boolean update(Sale sale) {
// DB code
	    return Boolean.TRUE;
    }

    @Override
    public long count() {

        // DB code
        return 0;
    }

    @Override
    public Sale insert(Sale value) {
        // DB code
        return null;
    }

    @Override
    public Sale getById(Long key) {
        // DB code
        return null;
    }
}
