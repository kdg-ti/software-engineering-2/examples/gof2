package demo.pos.persistence;

import demo.infra.Repository;
import demo.pos.domain.product.ProductDescription;

/**
 * @author Jan de Rijke.
 */
public interface ProductRepository extends Repository<Long,ProductDescription> {
}
