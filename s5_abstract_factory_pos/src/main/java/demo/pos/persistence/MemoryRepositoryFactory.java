package demo.pos.persistence;

/**
 * @author Jan de Rijke.
 */
public class MemoryRepositoryFactory implements RepositoryFactory {
	private  SaleRepository saleRepository = new SaleMemoryRepository();
	private ProductRepository catalog = new ProductDescriptionMemoryRepository();

	@Override
	public SaleRepository getSaleRepository(){
		return saleRepository;
	}

	@Override
	public ProductRepository getProductRepository(){
		return catalog;
	}

}