package demo.pos.business;

import demo.pos.persistence.Repositories;
import demo.pos.domain.product.ProductDescription;
import demo.pos.persistence.ProductRepository;


/**
 * Created by overvelj on 14/11/2016.
 */
public class ProductService {
    private ProductRepository products = Repositories.getFactory().getProductRepository();


    public ProductDescription addProduct(ProductDescription pd) {
        return products.insert( pd);
    }

    public ProductDescription getProductDesc(long id) {
        return products.getById(id);
    }


}
