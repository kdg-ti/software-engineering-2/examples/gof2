package staticfactory;

public class NationalRegistryNumberFactory {
    private NationalRegistryNumberFactory() {
        // empty
    }

    static public NationalRegisterNumber newNumber(String nummer) {
        // TODO: validate number
        return new BelgianNationalRegisterNumber(nummer);
    }
}

/*
How to check a NationalInsuranceNumber?
Verify that the check digits (the last two digits) are correct.
The check digit need to be the complement of 97 of the first 9 numbers module 97.

For the caclulation add a 2 in front of the first 9 numbers if the person is born after
31/12/1999

Person born before 1/1/2000:
NIN = 72020290081: 97 - (modulo 97 of 720202900) = 97 - 16 = 81

Person born before 31/12/1999
NIN = 00012556777: 97 - (modulo 97 van 2000125567) = 97 - 20 = 77
*/
