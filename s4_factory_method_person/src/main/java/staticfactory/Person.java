package staticfactory;

public interface Person {
    String getName();

    NationalRegisterNumber getNumber();
}

