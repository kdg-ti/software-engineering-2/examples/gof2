package staticfactory;

public interface NationalRegisterNumber {
    NationalRegisterNumber INVALID_NUMBER = NationalRegistryNumberFactory.newNumber("00.00.00 000-00");

    String toString();

    long toLong();
}

