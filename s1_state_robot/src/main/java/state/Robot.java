package state;

public class Robot implements RobotState {
	private RobotState state;

	public Robot(){
		setState(new StateOff(this));
	}

	public void setState(RobotState state){
		this.state = state;
	}

	@Override
	public void walk() {
		state.walk();
	}

	@Override
	public void cook() {
		state.cook();
	}

	@Override
	public void off() {
		state.off();
	}

	public RobotState getState() {
		return state;
	}

}