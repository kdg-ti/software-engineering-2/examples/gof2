package static_factory_method_refactor.static_factory_method;

public class Student {
  int id;
  String name;
  double score;
  private Student(int id, String name, double score) {
    this.id = id;
    this.name = name.trim();
    this.score = score;
  }

  public static Student createStudent(int id, String name, double score) {
    return new Student(id, name, score);
  }
}
