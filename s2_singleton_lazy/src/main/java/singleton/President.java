package singleton;

/**
 * Singleton example with lazy initialization
 */
public final class President {
    private static President instance;
    private String name;
    private String party;

    // private constructor!
    private President(String name, String party) {
        this.name = name;
        this.party = party;
    }

    public static synchronized President getInstance() {
        if (instance == null) {
            instance = new President("unknown", "unknown");
        }
        return instance;
    }

    public String getName() {
        return name;
    }

    public String getParty() {
        return party;
    }

    public void setPresident(String naam, String party) {
        this.name = naam;
        this.party = party;
    }

    @Override
    public String toString() {
        return "President: " + name + ", party: " + party;
    }

}