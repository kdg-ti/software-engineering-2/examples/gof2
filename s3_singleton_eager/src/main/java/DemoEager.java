import singleton.President;

public class DemoEager {
    public static void main(String[] args) {
        //President test = new President("Donald Trump", "Republicans");

        President p = President.getInstance();
        p.setPresident("Donald Trump", "Republicans");
        System.out.println(p);

        President np = President.getInstance();
        np.setPresident("Joe Biden", "Democrats");
        System.out.println(p);
        System.out.println(np);
    }
}
